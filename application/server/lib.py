from flask_wtf import FlaskForm
from wtforms import IntegerField
from wtforms import StringField
from wtforms.validators import DataRequired


def create_system_model(database, table_name):
    class SystemFormModel(database.Model):
        __tablename__ = table_name
        id = database.Column(database.Integer, primary_key=True)
        range_start = database.Column(database.String)
        range_end = database.Column(database.String)
        AS_number = database.Column(database.Integer)
        country_code = database.Column(database.String)
        AS_description = database.Column(database.String)
    return SystemFormModel


class SystemForm(FlaskForm):
    id = IntegerField("id", validators=[DataRequired()])
    range_start = StringField("range_start", validators=[DataRequired()])
    range_end = StringField("range_end", validators=[DataRequired()])
    AS_number = IntegerField("AS_number", validators=[DataRequired()])
    country_code = StringField("country_code", validators=[DataRequired()])
    AS_description = StringField("AS_description", validators=[DataRequired()])
