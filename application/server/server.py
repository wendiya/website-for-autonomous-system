import os
from ipaddress import IPv4Address
from ipaddress import IPv6Address
from ipaddress import ip_address
from socket import AF_INET
from socket import AF_INET6
from socket import inet_pton

from flask import Flask
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy

from application.server.lib import create_system_model
from application.server.exceptions import PageNotFound
from application.server.exceptions import UnknownIpAddressType


class Server:
    def __init__(self):
        self._app = Flask(__name__)
        self._system_model = None
        self._database = None

    @property
    def app(self):
        return self._app

    def define_configurations(self, path_to_database):
        self._app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{path_to_database}'
        self._app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self._app.config['SECRET_KEY'] = os.urandom(32)

    def define_database_connection(self):
        self._database = SQLAlchemy(self._app)
        self._system_model = create_system_model(database=self._database, table_name='ip2asn')
        self._database.create_all()

    def define_routers(self):
        @self._app.route('/autonomousSystem/<int:identifier>', methods=['GET'])
        def get_description_by_number(identifier):
            information = self._system_model.query.filter_by(AS_number=identifier).first()

            if information is not None:
                response = {identifier: information.AS_description}
                return jsonify(response), 200
            else:
                raise PageNotFound()

        @self._app.route('/autonomousSystem/<ipAddress>', methods=['GET'])
        def get_description_by_address(ipAddress):
            resolved_ip_address = resolve_ip_type(ipAddress)
            if resolved_ip_address is None:
                raise UnknownIpAddressType()

            information = self._system_model.query.filter(
                self._system_model.range_start <= resolved_ip_address
            ).filter(self._system_model.range_end >= resolved_ip_address
                     ).first()

            if information is not None:
                response = {ipAddress: information.AS_description}
                return jsonify(response), 200
            else:
                raise PageNotFound()


def resolve_ip_type(address):
    if type(ip_address(address)) is IPv4Address:
        resolved_address = inet_pton(AF_INET, address)

    elif type(ip_address(address)) is IPv6Address:
        resolved_address = inet_pton(AF_INET6, address)

    else:
        resolved_address = None

    return resolved_address




