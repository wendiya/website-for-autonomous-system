class PageNotFound(Exception):
    status_code = 404

    def __init__(self, payload=None):
        Exception.__init__(self)
        self.message = "Page is not found"
        self.status_code = 404
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class UnknownIpAddressType(Exception):
    status_code = 400

    def __init__(self, payload=None):
        Exception.__init__(self)
        self.message = "IP Address has unknown type"
        self.status_code = 400
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv
