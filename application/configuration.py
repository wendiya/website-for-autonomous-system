import json


class Configuration:
    def __init__(self, config_file_name):
        with open(config_file_name) as config:
            json_data = json.load(config)

        self.path_to_tsv = json_data['path_to_tsv']
        self.path_to_database = json_data['path_to_database']
