import csv
from ipaddress import IPv4Address
from ipaddress import IPv6Address
from ipaddress import ip_address
from socket import AF_INET
from socket import AF_INET6
from socket import inet_pton
from pathlib import Path

from loguru import logger
import sqlite3

from application.database.queries import CREATE_TABLE
from application.database.queries import INSERT_RECORDS


def create_database(path_to_database, path_to_tsv):
    try:
        file = open(path_to_tsv)

    except:
        raise ValueError(f'Can not find tsv file. '
                         f'Check path correctness in the configuration: {path_to_tsv}')

    Path(path_to_database).touch()
    contents = csv.reader(file, delimiter='\t')

    connection = sqlite3.connect(path_to_database)
    cursor = connection.cursor()

    try:
        cursor.execute(CREATE_TABLE)

    except:
        logger.warning(f'Can not create table, because it already exists')

    for content in contents:

        if type(ip_address(content[0])) is IPv4Address and type(ip_address(content[1])) is IPv4Address:
            content[0] = inet_pton(AF_INET, content[0])
            content[1] = inet_pton(AF_INET, content[1])

        elif type(ip_address(content[0])) is IPv6Address and type(ip_address(content[1])) is IPv6Address:
            content[0] = inet_pton(AF_INET6, content[0])
            content[1] = inet_pton(AF_INET6, content[1])

        else:
            continue

        cursor.execute(INSERT_RECORDS, content)

    connection.commit()
    connection.close()
