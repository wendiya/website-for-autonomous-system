CREATE_TABLE = '''CREATE TABLE ip2asn(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                range_start TEXT NOT NULL,
                range_end TEXT NOT NULL,
                AS_number INTEGER NOT NULL,
                country_code TEXT NOT NULL,
                AS_description TEXT NOT NULL);
                '''

INSERT_RECORDS = "INSERT INTO ip2asn (range_start, range_end, AS_number, " \
                 "country_code, AS_description) VALUES(?, ?, ?, ?, ?)"
