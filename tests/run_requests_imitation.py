import os
import sys

import requests

ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if ROOT not in sys.path:
    sys.path.append(ROOT)


def main():
    address_template = 'http://127.0.0.1:5000/autonomousSystem/{}'

    tests = [['1.0.0.0', '13335', 'CLOUDFLARENET'],
             ['1.0.3.255', '0', 'Not routed'],
             ['1.0.5.255', '38803', 'WPL-AS-AP Wirefreebroadband Pty Ltd'],
             ['1.0.6.0', '38803', 'WPL-AS-AP Wirefreebroadband Pty Ltd'],
             ['1.0.63.255', '0', 'Not routed'],
             ['1.0.64.0', '18144', 'AS-ENECOM Energia Communications,Inc.'],
             ['1.0.141.255', '23969', 'TOT-NET TOT Public Company Limited'],
             ['1.0.142.0', '23969', 'TOT-NET TOT Public Company Limited'],
             ['1.0.255.255', '23969', 'TOT-NET TOT Public Company Limited'],
             ['1.1.0.0', '0', 'Not routed'],
             ['ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff', '0', 'Not routed'],
             ['fec0::', '0', 'Not routed'],
             ['febf:ffff:ffff:ffff:ffff:ffff:ffff:ffff', '0', 'Not routed'],
             ['fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff', '0', 'Not routed'],
             ['2c0f:fff0::', '37125', 'Layer3-'],
             ['2c0f:ffd1::', '0', 'Not routed'],
             ['2c0f:ffd0:ffff:ffff:ffff:ffff:ffff:ffff', '36968', 'ECN-AS1'],
             ['2c0f:ffc9::', '0', 'Not routed'],
             ['2c0f:ffc8:ffff:ffff:ffff:ffff:ffff:ffff', '22355', 'FROGFOOT'],
             ['2c0f:ffc8::', '22355', 'FROGFOOT']]

    for test in tests:
        response = requests.get(address_template.format(test[1]))
        text = response.json()

        assert 200 == response.status_code
        assert test[2] == text[test[1]]

    for test in tests:
        response = requests.get(address_template.format(test[0]))
        text = response.json()

        assert 200 == response.status_code
        assert test[2] == text[test[0]]


if __name__ == "__main__":
    main()
